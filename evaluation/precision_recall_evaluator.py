__author__ = 'Kartik Perisetla(kperisetla@cmu.edu)'

from base_evaluator import BaseEvaluator
import logging

class PrecisionRecallEvaluator(BaseEvaluator):
    """
    class to compute Precision(P), Recall(R), F1 score, Confusion Matrix
    and accuracy of the model
    """
    def __init__(self, file_name=".temp.log"):
        self.file_name = file_name
        self._buff=''

    def save_buff(self):
        f = open(self.file_name,"w")
        f.write(self._buff)
        f.close()

    def _get_matching_label_count(self, test_labels, indices, label_value):
        """
        get count for number of instances where predicted_labels matched
        test_labels with value predicted_label
        :param test_labels:
        :param predicted_labels:
        :param predicted_label:
        :return: count where predicted label and actual label are same and are
        equal to label_value
        """
        count = 0
        for index in indices:
            if test_labels[index] == label_value:
                count += 1
        return count

    def get_precision_recall_f1(self, test_set_labels, predicted_set_labels):
        """
        method to compute the precision, recall and F1 score of
        classification task
        :param test_set_labels:
        :param predicted_set_labels:
        :param print_prf: set True if want to get P, R and F1 scores printed to console
        :return: tuple of precision, recall and f1 dictionaries
        """
        label_set = set(test_set_labels)
        precision = {}
        recall = {}
        f1 = {}

        print "\n\n","*"*25,"P/R/F1","*"*20
        self._buff += "\n\n"+"*"*25+"P/R/F1"+"*"*20+"\n"

        for label_a in label_set:
            label_indices = [index for index,item in
                             enumerate(predicted_set_labels) if item == label_a]
            true_positive_plus_false_positive = len(label_indices)
            true_positive = self._get_matching_label_count(test_set_labels,
                                                          label_indices,
                                                          label_a)

            if true_positive_plus_false_positive == 0:
                prec_a = 0
            else:
                prec_a = round(float(true_positive)/
                               true_positive_plus_false_positive, 2)
            print "*"*55
            self._buff += "*"*55+"\n"

            print "Precision[",label_a,"]=",\
                prec_a, \
                "[tp=",true_positive," tp+fp=",\
                true_positive_plus_false_positive,"]"
            self._buff += "Precision["+str(label_a)+"]="+\
                str(prec_a)+ \
                "[tp="+str(true_positive)+" tp+fp="+\
                str(true_positive_plus_false_positive)+"]"+"\n"

            precision[label_a] = prec_a

            actual_label_indices = [index
                                    for index,item in
                                    enumerate(test_set_labels)
                                    if item == label_a]
            true_positive_plus_false_negative = len(actual_label_indices)

            if true_positive_plus_false_negative == 0:
                rec_a = 0
            else:
                rec_a = round(float(true_positive)/
                              true_positive_plus_false_negative, 2)
            print "Recall[",label_a,"]=",\
                    rec_a,\
                    "[tp=",true_positive," tp+fn=",\
                true_positive_plus_false_negative,"]"
            self._buff += "Recall["+str(label_a)+"]="+\
                    str(rec_a)+\
                    "[tp="+str(true_positive)+" tp+fn="+\
                str(true_positive_plus_false_negative)+"]"+"\n"

            recall[label_a] = rec_a

            if prec_a == 0 or rec_a == 0:
                f1_a = 0
            else:
                f1_a = round(float(2 * prec_a * rec_a)/(prec_a + rec_a), 2)

            print "F1[",label_a,"]=",f1_a
            self._buff += "F1["+str(label_a)+"]="+str(f1_a)+"\n"
            f1[label_a] = f1_a

        return (precision, recall, f1)

    def get_confusion_matrix(self, test_set_labels, predicted_set_labels, ids=None, get_ids=False):
        """
        method to generate confusion matrix
        :param test_set_labels:
        :param predicted_set_labels:
        :param ids: id value associated with instance
        :param get_ids: set True if ids of instances falling each cell of confusion matrix is required
        :return: confusion matrix of type (dictionary of dictionaries)
        """
        label_set = set(test_set_labels)
        confusion_matrix = {}
        actual_label_count = {}
        cell_ids = {}
        for actual_label in label_set:
            cnt = len([index for index,item in
                             enumerate(test_set_labels)
                             if item == actual_label])
            actual_label_count[actual_label] = cnt

        for predicted_label in label_set:
            confusion_matrix[predicted_label] = {}
            if get_ids:
                cell_ids[predicted_label] = {}
            predicted_label_indices = [index for index,item in
                             enumerate(predicted_set_labels)
                             if item == predicted_label]
            for actual_label in label_set:
                actual_label_indices = [index for index,item in
                             enumerate(test_set_labels)
                             if item == actual_label]
                # find where indices intersect i.e common in both
                intersection = set(actual_label_indices) \
                               & set(predicted_label_indices)
                intersection_count = len(intersection)

                confusion_matrix[predicted_label][actual_label] =\
                    intersection_count

                if get_ids:
                    _ids = []
                    for index in intersection:
                        _ids.append(ids[index])
                    cell_ids[predicted_label][actual_label] = _ids

        print "*"*15,"Actual instance count","*"*15
        self._buff += "*"*15+"Actual instance count"+"*"*15+"\n"

        for label, count in actual_label_count.iteritems():
            print label,":",count,"\t",
            self._buff += str(label)+":"+str(count)+"\t"

        print "\n","*"*15,"Confusion Matrix","*"*15
        self._buff += "\n"+"*"*15+"Confusion Matrix"+"*"*15+"\n"

        print "(Actual)\t",
        self._buff += "(Actual)\t"

        # printing confusion matrix
        for label in confusion_matrix.keys():
            print " ",label,
            self._buff += " "+str(label)

        print "\n","*"*50
        self._buff += "\n"+"*"*50+"\n"

        print "(Predicted)",
        self._buff += "(Predicted)"

        for predicted_label, confusion_submatrix in confusion_matrix.iteritems():
            print "\n",predicted_label,
            self._buff += "\n"+str(predicted_label)

            for actual_label, count in confusion_submatrix.iteritems():
                print "\t",count,
                self._buff += "\t"+str(count)

        # output ids if asked to print instance ids falling in each cell
        if get_ids:
            print "\n","*"*50
            print "instance ids falling in each cell"
            for key,val in cell_ids.iteritems():
                for okey, lst in val.iteritems():
                    print "\n[",key,"][",okey,"]=",lst

        return confusion_matrix

    def get_accuracy(self, confusion_matrix):
        """
        method to get the accuracy for each class
        :param test_set_labels:
        :param predicted_set_labels:
        :return: accuracy of the model
        """
        label_set = set(confusion_matrix.keys())
        all_true_positives = 0
        for label in label_set:
            all_true_positives += confusion_matrix[label][label]

        total = 0
        for pred_label, confusion_submatrix in confusion_matrix.iteritems():
            for act_label, count in confusion_submatrix.iteritems():
                total += count

        print "\n","*"*15,"Model Accuracy","*"*15
        self._buff += "\n"+"*"*15+"Model Accuracy"+"*"*15+"\n"

        accuracy = round((float(all_true_positives)*100)/total, 2)
        print accuracy,"% [correctly predicted ",all_true_positives," of ",total," instances]"
        self._buff += str(accuracy)+"% [correctly predicted "+str(all_true_positives)+" of "+str(total)+" instances]"+"\n\n"


        return accuracy

    def evaluate(self, pred_labels, true_labels, ids= None, get_ids=False):
        """
        method to evaluate the model
        :param pred_labels:
        :param true_labels:
        :return:
        """
         # get P/R/F1 score
        self.get_precision_recall_f1(true_labels, pred_labels)

        # get confusion matrix for this specific run
        confusion_matrix = self.get_confusion_matrix(true_labels, pred_labels,ids=ids, get_ids=get_ids)

        # get model accuracy
        self.get_accuracy(confusion_matrix)

        self.save_buff()

