__author__ = 'Erika Menezes(emenezes@andrew.cmu.edu)'

class BaseEvaluator(object):
    def __init__(self):
        """
        for necessary initilization
        :return:
        """
        pass


    def evaluate(self, pred_labels, true_labels):
        """
        function to print evaluation metric for a classifier
        :param pred_labels: predicted labels by classifier on test set
        :param true_labels: true labels of test set
        :return:
        """
        print "BaseEvaluation:evaluate:derived class need to" \
              "override this method"