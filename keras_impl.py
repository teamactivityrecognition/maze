__author__ = 'Kartik Perisetla(kperisetla@cmu.edu)'

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
import numpy as np

import skoda
from skoda_mini_preprocess import *

class KerasModel:
    def __init__(self, max_features = 192, maxlen = 192):
        print "KerasModel:init"
        self.model = Sequential()
        self.model.add(Embedding(max_features, 256, input_length=maxlen))
        self.model.add(LSTM(output_dim=128, activation='sigmoid', inner_activation='hard_sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(1))
        self.model.add(Activation('sigmoid'))
        print "added model attributes, about to compile"
        self.model.compile(loss='binary_crossentropy', optimizer='rmsprop')
        print "model compiled."

    def train(self, X_train, Y_train):
        print "about to train the model"
        self.model.fit(X_train, Y_train, shuffle=False, nb_epoch=2)
        print "model trained on :",len(X_train)


    def predict(self, X_test, Y_test):
        score = self.model.evaluate(X_test, Y_test)
        print "score:",score


X_train, X_test, y_train, y_test = get_train_test_split(0.2)
X_train = X_train[0:5000]
# X_test = X_test[0:5000]
y_train = y_train[0:5000]
# y_test = y_test[0:5000]

k= KerasModel()
k.train(np.array(X_train), np.array(y_train))
k.predict(np.array(X_test), np.array(y_test))
