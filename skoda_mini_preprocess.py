from sklearn.cross_validation import train_test_split
import pickle as pkl

def get_train_test_split(test_perc):
    """
    function to get train-test split
    :param test_perc: percentage of test data
    :return:
    """
    fobj = open("sensor_16_processed.dat","r")
    X = []
    y = []
    for line in fobj:
        line = line.replace("\n","")
        items = line.split(" ")
        _vector = []
        for i in range(0,193):
            _vector.append(float(items[i]))

        _label = int(items[192])
        X.append(_vector)
        y.append(_label)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_perc, random_state=42)
    print "prepared train and test set."
    return (X_train,X_test,y_train,y_test)


def main():
    X_train, X_test, y_train, y_test = get_train_test_split(0.2)
    f = open('skoda_mini.pkl', 'wb')
    pkl.dump((X_train, y_train), f, -1)
    pkl.dump((X_test, y_test), f, -1)
    f.close()
    print "skoda_mini_preprocess: pickled train and test set"


if __name__ == '__main__':
    main()
