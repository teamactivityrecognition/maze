import time 
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from numpy import genfromtxt, savetxt
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score

def get_x_data():
	myfile = open('/home/vj/Downloads/sensor_16_processed.dat', 'r')
	x=[]
	y=[]
	for line in myfile:
        	line = line.replace("\n","")
        	items = line.split(" ")
        	label = items[192]
        	vector = items[0:192]
        	x.append(vector)
        	y.append(label)
	myfile.close()
	return (x, y)	

def get_x_y_data():
	myfile = open('/home/vj/Downloads/sensor_16_processed.dat', 'r')
	x=[]
	y=[]
	for line in myfile:
        	line = line.replace("\n","")
        	items = line.split(" ")
        	label = items[192]
        	vector = items[0:192]
        	x.append(vector)
        	y.append(label)
	myfile.close()
	train_x, test_x, train_y, test_y  = train_test_split(x, y, test_size=0.2, random_state=0)
	return (train_x, train_y, test_x, test_y)

def train():
	train, train_labels = get_x_data()
	rf = RandomForestClassifier(max_depth=None, min_samples_split=1)
	rf.fit(train, train_labels)
	predicted_labels = rf.predict(train)
	train_accuracy = accuracy_score(train_labels, predicted_labels)
	print("train accuracy:", train_accuracy)

def main():
	#create the training & test sets, skipping the header row with [1:]
	train, train_labels, test, test_labels = get_x_y_data()

	#create and train the random forest
	#multi-core CPUs can use: rf = RandomForestClassifier(n_estimators=100, n_jobs=2)
	rf = RandomForestClassifier(max_depth=None, min_samples_split=1)
	rf.fit(train, train_labels)
	predicted_labels = rf.predict(test)
	accuracy = accuracy_score(test_labels, predicted_labels)
	print("test accuracy:", accuracy)
	
	F1=f1_score(test_labels,predicted_labels,average='macro')
	print "f1 score:", (F1)

def cross_validation():
	train, train_labels, test, test_labels = get_x_y_data()
	rf = RandomForestClassifier(max_depth=None, min_samples_split=1)
	scores = cross_val_score(rf, train, train_labels, cv=5)
	print "cross val score:", (scores.mean())

if __name__=="__main__":
	print (time.strftime("%H:%M:%S"))    
	main()
	#train()
	#cross_validation()
	print (time.strftime("%H:%M:%S"))


