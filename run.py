__author__ = 'Kartik Perisetla(kperisetla@cmu.edu)'
from MultiLayerPerceptron.MLP import MLPAlgorithm


if __name__=="__main__":
    algo = MLPAlgorithm(use_model=False)
    algo.set_num_iterations(50)
    algo.execute()