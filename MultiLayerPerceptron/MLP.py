from sklearn.cross_validation import train_test_split
from keras.models import Sequential
from keras.models import *
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import SGD
import keras
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from keras.utils import np_utils
import random
import numpy as np
import sys, os

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print "PATH:",path
sys.path.append(path)

from evaluation.precision_recall_evaluator import PrecisionRecallEvaluator

class MLPAlgorithm(object):
    """
    class that implements algorithm with multilayer perceptron
    """

    def __init__(self, use_model=False):
        self.use_model_flag = use_model
        self.prepare_network()

    def set_num_iterations(self, num=10):
        """
        method to set the number of iterations to train ANN
        :param num:
        :return:
        """
        self.iterations = num

    def get_training_data(self):
        """
        method to read the train and test set
        :return:
        """
        myfile = open('sensor_16_processed.dat', 'r')
        x=[]
        y=[]
        for line in myfile:
            line = line.replace("\n","")
            items = line.split(" ")
            label = int(items[192])
            vector = items[0:192]
            x.append(vector)
            y.append(label)
        myfile.close()
        return (x, y)

    def use_param1(self):
        print self.__class__.__name__,":using  param1"
        self.model.add(Dense(64, input_dim=192, init='uniform'))
        self.model.add(Activation('tanh'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(64, init='uniform'))
        self.model.add(Activation('tanh'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(1, init='uniform'))
        self.model.add(Activation('softmax'))

        sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
        self.model.compile(loss='mean_squared_error', optimizer=sgd)

    def use_param2(self):
        print self.__class__.__name__,":using  param2"
        self.model.add(Dense(64, input_dim=192, init='zero'))
        self.model.add(Activation('sigmoid'))
        self.model.add(Dropout(0.001))
        self.model.add(Dense(64, init='zero'))
        self.model.add(Activation('sigmoid'))
        self.model.add(Dropout(0.001))
        self.model.add(Dense(1, init='zero'))
        self.model.add(Activation('sigmoid'))

        adagrad = keras.optimizers.Adagrad(lr=0.01, epsilon=1e-6)
        self.model.compile(loss='mean_absolute_error', optimizer=adagrad)

    def use_param3(self):
        """
        method with params given by ming
        :return:
        """
        # try relu activation function as well

        print self.__class__.__name__,":using  param3"
        self.model.add(Dense(1024, input_dim=192))
        self.model.add(Activation('sigmoid'))
        self.model.add(Dense(64, input_dim=1024))
        self.model.add(Activation('sigmoid'))
        self.model.add(Dense(10, input_dim=64))
        self.model.add(Activation('softmax'))

        sgd = SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
        self.model.compile(loss='categorical_crossentropy', optimizer=sgd)

    def use_param4(self):
        """
        method with params given by ming
        :return:
        """
        # try relu activation function as well

        print self.__class__.__name__,":using  param4"
        self.model.add(Dense(1024, input_dim=192))
        self.model.add(Activation('relu'))
        self.model.add(Dense(64, input_dim=1024))
        self.model.add(Activation('relu'))
        self.model.add(Dense(10, input_dim=64))
        self.model.add(Activation('softmax'))

        sgd = SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
        self.model.compile(loss='categorical_crossentropy', optimizer=sgd)

    def prepare_network(self):
        """
        method to prepare the ANN
        :return:
        """
        self.model = Sequential()
        # Dense(64) is a fully-connected layer with 64 hidden units.
        # in the first layer, you must specify the expected input data shape:
        # here, 20-dimensional vectors.

        # self.use_param1()

        self.use_param4()

    def execute(self):
        x, y = self.get_training_data()
        x = np.array(x)
        y = np.array(y)

        # apply transformation given by Ming
        y = np_utils.to_categorical(y, 10)

        # self.get_train_error(x, y)
        # self.get_80_20_accuracy(x, y)
        self.get_cross_validation_scores()

    def get_train_error(self, train_x, train_y):
        """
        method to get the train accuracy
        :param train_x:
        :param train_y:
        :return:
        """

        print "train error- train_x size:",train_x.size," train_y size:",train_y.size
        self.model.fit(train_x, train_y, shuffle=False, nb_epoch=self.iterations)
        predict_y = self.model.predict_classes(train_x, batch_size=32)

        # apply same transformation on predicted labels
        predict_y = np_utils.to_categorical(predict_y, 10)

        # print "prediction:",predict_y
        accuracy = accuracy_score(train_y, predict_y)
        print "train accuracy:", accuracy
        self.get_eval_metric(train_y, predict_y)

    def get_eval_metric(self, test_y, predicted_y):
        test_y_as_list = self.get_labels_as_list(test_y)
        prediction_as_list = self.get_labels_as_list(predicted_y)
        eval = PrecisionRecallEvaluator()
        eval.get_precision_recall_f1(test_y_as_list, prediction_as_list)
        confusion_matrix = eval.get_confusion_matrix(test_y_as_list, prediction_as_list)
        eval.get_accuracy(confusion_matrix)

    def get_labels_as_list(self, ndarr):
        result = []

        # convert ndarray to list such that we pick the max value from each
        # row list and get its index as class
        ndarr = ndarr.tolist()
        for row in ndarr:
            cls = row.index(max(row))
            result.append(cls)

        return result

    def get_80_20_accuracy(self, train_x, train_y):
        """
        method to get accuracy on 20% split using 80% train
        :param train_x:
        :param train_y:
        :return:
        """
        train_x, test_x, train_y, test_y = train_test_split(train_x, train_y, test_size=0.2, random_state=0)

        print "80/20 testint- train_x size:",train_x.size," train_y size:",train_y.size
        if not self.use_model_flag:
            print "train_x:",len(train_x)," train_y:",len(train_y)
            self.model.fit(train_x, train_y, shuffle=False, nb_epoch=self.iterations)
            self.save_model()
        else:
            self.load_model()
        predict_y = self.model.predict_classes(test_x, batch_size=32)
        predict_y = np_utils.to_categorical(predict_y, 10)
        print "prediction:",predict_y
        accuracy = accuracy_score(test_y, predict_y)
        print "80/20 split accuracy:", accuracy
        self.get_eval_metric(test_y, predict_y)

    def load_model(self):
        if not os.path.exists('mlp_model_architecture.json'):
            print self.__class__.__name__,"Load failed:file '",self.model_name,"' not found!"
            return
        # load the model from binary file
        self.model = model_from_json(open('mlp_model_architecture.json').read())
        self.model.load_weights('mlp_model_weights.h5')

        print self.__class__.__name__,": model loaded successfully"
        return self.model

    def save_model(self):
        """
        method to save the trained model on disk
        :return:
        """
        json_string = self.model.to_json()
        open('mlp_model_architecture.json', 'w').write(json_string)
        self.model.save_weights('mlp_model_weights.h5', overwrite=True)
        print self.__class__.__name__,": model saved successfully"

    def get_test_accuracy(self, train_x, train_y, test_x, test_y):
        """
        method to compute test accuracy
        :param train_x:
        :param train_y:
        :param test_x:
        :param test_y:
        :return:
        """
        # print "train_x size:",train_x.size," train_y size:",train_y.size
        self.model.fit(train_x, train_y, shuffle=True, nb_epoch=self.iterations)
        predict_y = self.model.predict_classes(test_x, batch_size=32)


        self.get_f1score(self.get_labels_as_list(test_y), predict_y)

        predict_y = np_utils.to_categorical(predict_y, 10)
        accuracy = accuracy_score(test_y, predict_y)
        print "test accuracy:", accuracy
        self.get_eval_metric(test_y, predict_y)
        return accuracy

    def get_f1score(self, test_y, predicted_y):
        """
        method to compute F1 score
        :param test_y:
        :param predicted_y:
        :return:
        """
        F1=f1_score(test_y,predicted_y,average='macro')
        print "f1 score:", (F1)
        return F1

    def get_shuffled_instances(self, instances, labels):

        new_lst = []
        for instance, label in zip(instances, labels):
            tpl = (instance, label)
            new_lst.append(tpl)

        random.shuffle(new_lst)

        instances = []
        labels = []
        for instance, label in new_lst:
            instances.append(instance)
            labels.append(label)

        return (instances, labels)
    def get_cross_validation_scores(self, k = 5, instances=None, labels=None):
        """
        method to do 10 fold cross validation
        :param instances:
        :param labels:
        :return:
        """

        if instances is None and labels is None:
            print "auto mode"
            instances, labels = self.get_training_data()

        # shuffle instances
        instances, labels = self.get_shuffled_instances(instances, labels)
        # by default using 10 fold cross validation
        i = 1
        fold_index = 0
        fold_size = len(instances)/k

        scores = []
        print "starting ",k,"-fold cross validation..."
        while i <=k:
            print "FOLD:",i
            _test = instances[fold_index : fold_index + fold_size]
            _test_labels = labels[fold_index : fold_index + fold_size]

            print "\ntest index:",fold_index,":",fold_index+fold_size, " LEN:",len(_test)

            _train = []
            _train_labels = []

            if fold_index + fold_size > len(instances):
                _train = instances[0:fold_index]
                _train_labels = labels[0:fold_index]
                print "last fold, train index:",0,":",fold_index, " LEN:",len(_train)
            elif fold_index == 0:
                _train = instances[fold_index + fold_size : ]
                _train_labels = labels[fold_index + fold_size : ]
                print "first fold, train index:",fold_index + fold_size,":all", " LEN:",len(_train)
            else:
                _train_l = instances[0 : fold_index]
                _train_l_labels = labels[0 : fold_index]

                _train_r = instances[fold_index + fold_size : ]
                _train_r_labels = labels[fold_index + fold_size : ]

                _train.extend(_train_l)
                _train.extend(_train_r)

                _train_labels.extend(_train_l_labels)
                _train_labels.extend(_train_r_labels)
                print "mid fold, train index:",0,":",fold_index," | ",fold_index + fold_size,":all", " LEN:",len(_train)

            # apply transformation given by Ming
            _train_labels = np_utils.to_categorical(_train_labels, 10)
            _test_labels = np_utils.to_categorical(_test_labels, 10)

            score = self.get_test_accuracy(np.array(_train), np.array(_train_labels),
                                           np.array(_test), np.array(_test_labels))

            # score = self.get_test_accuracy(_train, _train_labels,
            #                                _test, _test_labels)

            scores.append(score)
            i += 1
            fold_index += fold_size
            self.model = None
            # re-prepare the network
            self.prepare_network()

        print "scores=",scores
        print "avg. score=",sum(scores)/len(scores)
        return scores

