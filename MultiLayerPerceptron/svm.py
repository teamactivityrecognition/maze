from sklearn.cross_validation import train_test_split
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

def get_training_data():
    myfile = open('sensor_16_processed.dat', 'r')
    x=[]
    y=[]
    for line in myfile:
        line = line.replace("\n","")
        items = line.split(" ")
        label = items[192]
        vector = items[0:192]
        x.append(vector)
        y.append(label)
    myfile.close()
    train_x, test_x, train_y, test_y = train_test_split(x, y, test_size=0.2, random_state=0)
    return (train_x, test_x, train_y, test_y)


def train_error():
    train_x, test_x, train_y, test_y = get_training_data()
    clf = svm.SVC()
    clf.fit(train_x,train_y)
    predict_y=clf.predict(train_x)
    print "prediction:",predict_y

    accuracy = accuracy_score(train_y,predict_y)
    print "train accuracy:", (accuracy)

def test_accuracy():
    train_x, test_x, train_y, test_y = get_training_data()
    clf = svm.SVC()
    clf.fit(train_x,train_y)
    predict_y=clf.predict(test_x)
    print "prediction:",predict_y

    accuracy = accuracy_score(test_y,predict_y)
    print "test accuracy:", (accuracy)

def f1score():
    train_x, test_x, train_y, test_y = get_training_data()
    clf = svm.SVC()
    clf.fit(train_x,train_y)
    predict_y=clf.predict(test_x)

    F1=f1_score(test_y,predict_y,average='macro')
    print "f1 score:", (F1)

train_error()
test_accuracy()
f1score();









